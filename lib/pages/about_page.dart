import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'adaPage.dart';
import 'donation.dart';

class aboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigoAccent,
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        centerTitle: true,
        title: Text("about"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.circular(10.0),

                // button text
              ),
              child: SelectableText(
                'This is an open-source app intended to combat malware-infected PCs that manipulate the copied addresses from wallets/exchanges into a non-intended 3rd party address that belongs to scammers. It compares the addresses using text recognition and produces the matching result \n\nArt Credits:\n • Icon made by Gregor Cresnar from www.flaticon.com\n• Screenhots generator https://app.flycricket.com',
                style: TextStyle(fontSize: 20, color: Colors.black),
                textAlign: TextAlign.justify,
                maxLines: 10,
              ),
            ),
          ),
          Column(
            children: [
              TextButton(
                onPressed: () {
                  Get.to(() => DonationPage());
                },
                child: Image.asset(
                  'assets/BuyMeACoffee_blue@2x.png',
                  width: 300,
                ),
              ),
              TextButton(
                  onPressed: () {
                    Get.to(() => AdaPage());
                  },
                  child: Container(
                    width: 300,
                    height: 60,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(10.0),

                      // button text
                    ),
                    child: Text(
                      "Tip me some ADA :)",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 25, color: Colors.white),
                    ),
                  ))
            ],
          )
        ],
      ),
    );
  }
}
