import 'package:flutter/material.dart';

class AdaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigoAccent,
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        centerTitle: true,
        title: Text("Tip me Some ADA :)"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.circular(10.0),

                // button text
              ),
              child: SelectableText(
                'DdzFFzCqrhspJ22WCf9C9oZqBhL3uXa2LoX3hctqyPrCKQFxMJENPL3NYqr9xQUx9cN3h99yTXBikzL6Vn8AZZRBLUET69Xs8Nfz2RGs',
                style: TextStyle(fontSize: 30),
              ),
            ),
          )
        ],
      ),
    );
  }
}
