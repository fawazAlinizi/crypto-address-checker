import 'dart:async';
import 'dart:io';

import 'dart:convert';
import 'dart:typed_data';

import 'package:crypto_address_checker/services/ocr_service.dart';
import 'package:crypto_address_checker/services/utilities.dart';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

class Controller extends GetxController {
  RxString exchange_address = ''.obs;
  RxString wallet_address = ''.obs;

  RxBool _matching_result = false.obs;
  RxString tempDirectory = ''.obs;
  RxString exchange_address_pic = ''.obs;
  RxString wallet_address_pic = ''.obs;

  //data vars
  RxString crop_page_Data = "".obs;

  //screenshot page
  RxString placeholder = ''.obs;

  //camera page
  RxString takePicForPlatform = ''.obs;

  //txt recognition

  //TextDetector? textDetectorController;

  RxString walletAddressRecognitionResult = ''.obs;
  RxString exchangeAddressRecognitionResult = ''.obs;

  //crop
  String? picPathToCrop;
  String? picPathToCrop_mode;

  Rx<Uint8List> walletCroppedPicData = Uint8List(0).obs;
  Rx<Uint8List> exchangeCroppedPicData = Uint8List(0).obs;

  @override
  void onInit() async {
    this.tempDirectory.value = (await getTemporaryDirectory()).path;

    this.placeholder.value = await getImageFileFromAssets(
        'placeholder_def.png', this.tempDirectory.value);

    this.exchange_address_pic.value = this.placeholder.value;
    this.wallet_address_pic.value = this.placeholder.value;

    //OCR_process is given the placeholder pic when screenshot page start

    Uint8List data_test = await File(this.placeholder.value).readAsBytes();
    this.walletCroppedPicData.value = data_test;
    this.exchangeCroppedPicData.value = data_test;

    super.onInit();
  }

  void resetScreenshotPage() {
    this.exchangeCroppedPicData.value = Uint8List(0);
    this.walletCroppedPicData.value = Uint8List(0);
    this.wallet_address_pic.value = this.placeholder.value;

    this.exchange_address_pic.value = this.placeholder.value;

    this.walletAddressRecognitionResult.value = "";
    this.exchangeAddressRecognitionResult.value = "";
  }

  Future<String> get exchange_pic_recogntion_result_test async {
    // String result =  await OCR_process(crop_page_Data.value);

    this.exchangeAddressRecognitionResult.value = await OCR_process(
            this.exchangeCroppedPicData.value, this.tempDirectory.value)
        .then((value) {
      return value;
    }).catchError((e) {
      throw e.toString();
    });

    return this.exchangeAddressRecognitionResult.value;
  }

  Future<String> get wallet_pic_recogntion_result_test async {
    this.walletAddressRecognitionResult.value = await OCR_process(
            this.walletCroppedPicData.value, this.tempDirectory.value)
        .then((value) {
      return value;
    }).catchError((e) {
      throw e.toString();
    });

    return this.walletAddressRecognitionResult.value;
  }

  picPathToCropWithPlatform(String path, String platform) {
    this.picPathToCrop = path;
    this.picPathToCrop_mode = platform;
  }

  deleteAddressPageVars() {
    this.exchange_address.value = '';
    this.wallet_address.value = '';
  }

  RxBool Matcher(String exchange_address, String wallet_address) {
    if (exchange_address.isNotEmpty && wallet_address.isNotEmpty) {
      if (exchange_address == wallet_address) {
        return RxBool(true);
      } else {
        return RxBool(false);
      }
    } else {
      return RxBool(false);
    }
  }

  RxBool get addressPageMatchingResult {
    _matching_result =
        Matcher(this.exchange_address.value, this.wallet_address.value);
    return _matching_result;
  }

  RxBool get screenshotPageMatchingResult {
    return Matcher(this.exchangeAddressRecognitionResult.value,
        this.walletAddressRecognitionResult.value);
  }

  Future<File?> getImageFromGallery(String platform) async {
    File? _imageFile;

    try {
      await ImagePicker().pickImage(source: ImageSource.gallery).then((image) {
        _imageFile = File(image!.path);
      });

      switch (platform) {
        case 'exchange':
          this.exchange_address_pic.value = _imageFile!.path;

          this.exchangeCroppedPicData.value =
              await File(_imageFile!.path).readAsBytes();

          break;

        case "wallet":
          this.wallet_address_pic.value = _imageFile!.path;

          this.walletCroppedPicData.value =
              await File(_imageFile!.path).readAsBytes();

          break;
      }
    } catch (e) {}
  }
}
