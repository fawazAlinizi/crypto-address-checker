import 'dart:io';

import 'package:camerawesome/camerawesome_plugin.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../state management/controller.dart';
import 'screenshot_input.dart';

// A screen that allows users to take a picture using a given camera.
class TakePicturePage extends StatefulWidget {
  @override
  TakePicturePageState createState() => TakePicturePageState();
}

class TakePicturePageState extends State<TakePicturePage> {
  late Controller controller;

  late ValueNotifier<CameraFlashes> _switchFlash;
  late ValueNotifier<Sensors> _sensor;
  late ValueNotifier<CaptureModes> _captureMode;
  late ValueNotifier<Size> _photoSize;

  // Controllers
  late PictureController _pictureController;

  @override
  void initState() {
    super.initState();
    //launching camera settings

    _switchFlash = ValueNotifier(CameraFlashes.NONE);
    _sensor = ValueNotifier(Sensors.BACK);
    _captureMode = ValueNotifier(CaptureModes.PHOTO);
    _photoSize = ValueNotifier(Size(500, 500));
    _pictureController = PictureController();

    //launching controller
    controller = Get.find();
  }

  @override
  void dispose() {
    _captureMode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width *
        MediaQuery.of(context).devicePixelRatio;
    double screenHeight = MediaQuery.of(context).size.height *
        MediaQuery.of(context).devicePixelRatio;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black54,
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Text(
                  "settings".tr,
                ),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                leading: Icon(Icons.info, color: Colors.white),
                title: Text("about"),
                onTap: () {
                  Get.back();

                  //  Get.to(() => aboutPage());
                },
              ),
              ListTile(
                leading: Icon(Icons.info, color: Colors.white),
                title: Text("rate_app"),
                onTap: () {
                  Get.back();
                },
              )
            ],
          ),
          // Populate the Drawer in the next step.
        ),
        appBar: AppBar(
          centerTitle: true,
          title: Text("Take a Picture"),
          backgroundColor: Colors.indigo,
        ),

        bottomNavigationBar: BottomAppBar(
          color: Colors.indigo,
          notchMargin: 5,
          child: Container(
            height: 70,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  child: Icon(
                    Icons.camera_alt_outlined,
                    color: Colors.white,
                    size: 50,
                  ),
                  onPressed: () async {
                    // c.resetController();
                    // c.delete_image_cach();

                    String picPath =
                        '${controller.tempDirectory.value}/${DateTime.now().millisecondsSinceEpoch}.png';

                    //c.cache_image_path = thisPath;
                    EasyLoading.showToast("loading".tr);

                    await _pictureController.takePicture(picPath);

                    switch (controller.takePicForPlatform.value) {
                      case 'exchange':
                        controller.exchange_address_pic.value = picPath;

                        controller.exchangeCroppedPicData.value =
                            await File(picPath).readAsBytes();

                        break;

                      case "wallet":
                        controller.wallet_address_pic.value = picPath;

                        controller.walletCroppedPicData.value =
                            await File(picPath).readAsBytes();

                        break;
                    }

                    EasyLoading.dismiss();

                    Get.to(() => ScreenShotPage());
                  },
                ),
              ],
            ),
          ),
        ),
        // Wait until the controller is initialized before displaying the
        // camera preview. Use a FutureBuilder to display a loading spinner
        // until the controller has finished initializing.
        body: Container(
          width: screenWidth,
          height: screenHeight,
          child: CameraAwesome(
            testMode: false,
            selectDefaultSize: (List<Size> availableSizes) => Size(1920, 1080),
            zoom: ValueNotifier(0.64),
            sensor: _sensor,
            photoSize: _photoSize,
            switchFlashMode: _switchFlash,
            captureMode: _captureMode,
            orientation: DeviceOrientation.portraitUp,
            fitted: true,
          ),
        ),
      ),
    );
  }
}
