import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

showSnackBar(String msg) async {
  Get.snackbar('', '',
      messageText: Text(
        msg,
        style: TextStyle(
          fontSize: 17,
        ),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.white,
      colorText: Colors.black,
      duration: Duration(seconds: 5),
      margin: EdgeInsets.only(top: 100),
      isDismissible: true,
      maxWidth: 400);
}

JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
  return JavascriptChannel(
      name: 'Toaster',
      onMessageReceived: (JavascriptMessage message) {
        // ignore: deprecated_member_use
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(message.message)),
        );
      });
}

const String KofiPage_html = '''
<!DOCTYPE html><html>
<head><title>Navigation Delegate Example</title></head>
<body>
<iframe src='https://ko-fi.com/fawazalinizi/?hidefeed=true&widget=true&embed=true&preview=true' style='border:none;width:100%;padding:4px;background:#f9f9f9;' height='712' title='fawazalinizi'></iframe>
</body>
</html>
''';

void kofiPage(
    Future<WebViewController> controller, BuildContext context) async {
  final String contentBase64 =
      base64Encode(const Utf8Encoder().convert(KofiPage_html));
  WebViewController webViewController = await controller;
  await webViewController.loadUrl('data:text/html;base64,$contentBase64');
}

class DonationPage extends StatefulWidget {
  @override
  _DonationPageState createState() => _DonationPageState();
}

class _DonationPageState extends State<DonationPage> {
  final CookieManager cookieManager = CookieManager();

  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    super.initState();

    //if (Platform.isMacOS) WebView.platform = CupertinoPopupSurface();

    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();

    kofiPage(_controller.future, context);
  }

  @override
  void dispose() async {
    super.dispose();
    try {
      cookieManager.clearCookies();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Buy me Coffee'),
        ),
        body: Builder(builder: (BuildContext context) {
          return WebView(
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            javascriptChannels: <JavascriptChannel>{
              _toasterJavascriptChannel(context),
            },
            gestureNavigationEnabled: true,
          );
        }));
  }
}
