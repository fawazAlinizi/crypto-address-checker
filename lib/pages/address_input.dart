import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../state management/controller.dart';
import 'about_page.dart';
import 'adaPage.dart';
import 'donation.dart';
import 'screenshot_input.dart';

class AddressPage extends StatefulWidget {
  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  Controller controller = Get.find();
  AnimationController? pulseAnimateController;
  AnimationController? flashAnimateController;

  @override
  void initState() {
    controller = Get.find();

    super.initState();
  }

  @override
  void dispose() {
    //pulseAnimateController?.dispose();
    //flashAnimateController?.dispose();

    print('it did dispose');
    pulseAnimateController?.dispose();
    flashAnimateController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          centerTitle: true,
          title: Text('Address Checker'),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: TextButton(
                child: Icon(
                  Icons.camera_alt_rounded,
                  size: 40,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.to(() => ScreenShotPage());

                  //because dispsoe overriding  doesnt work i put these here :)
                  controller.deleteAddressPageVars();
                },
              ),
            )
          ],
        ),
        backgroundColor: Colors.indigo,
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Text(
                  "Info",
                  style: TextStyle(fontSize: 50, color: Colors.white),
                ),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                leading: Icon(
                  Icons.info,
                  color: Colors.black,
                  size: 50,
                ),
                title: Text(
                  "about",
                  style: TextStyle(fontSize: 30),
                ),
                onTap: () {
                  Get.back();

                  Get.to(() => aboutPage());
                },
              ),
              ListTile(
                title: Container(
                    child: Image.asset('assets/BuyMeACoffee_blue@2x.png')),
                onTap: () {
                  Get.back();
                  Get.to(() => DonationPage());
                },
              ),
              ListTile(
                title: Container(
                  width: 300,
                  height: 55,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Text(
                    "tip me some ADA",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
                onTap: () {
                  Get.back();
                  Get.to(() => AdaPage());
                },
              )
            ],
          ),
          // Populate the Drawer in the next step.
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  children: [
                    Obx(
                      () => SelectableText(
                        'Exchange Address: Chars ${controller.exchange_address.value.length}',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    SingleChildScrollView(
                      child: TextField(
                        onChanged: (value) {
                          controller.exchange_address.value = value;
                        },
                        maxLines: 5,
                        autofocus: false,
                        showCursor: true,
                        style: TextStyle(fontSize: 30, color: Colors.black),
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.ltr,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  children: [
                    Obx(
                      () => Text(
                        'Wallet Address: Chars ${controller.wallet_address.value.length}',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    SingleChildScrollView(
                      child: TextField(
                        maxLines: 5,
                        autofocus: false,
                        showCursor: true,
                        style: TextStyle(fontSize: 30, color: Colors.black),
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.ltr,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                        ),
                        onChanged: (value) {
                          controller.wallet_address.value = value;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      Text(
                        'Do They Match? ',
                        style: TextStyle(fontSize: 25, color: Colors.white),
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Pulse(
                        infinite: true,
                        controller: (controller) =>
                            pulseAnimateController = controller,
                        child: Flash(
                          infinite: true,
                          controller: (controller) =>
                              flashAnimateController = controller,
                          child: GetX<Controller>(builder: (controller) {
                            return Text(
                              '${controller.addressPageMatchingResult.value.toString()}',
                              style: TextStyle(
                                  color:
                                      controller.addressPageMatchingResult.value
                                          ? Colors.green
                                          : Colors.red,
                                  fontSize: 25),
                            );
                          }),
                        ),
                      )
                    ])
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
