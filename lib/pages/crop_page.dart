import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:crop_your_image/crop_your_image.dart';
import 'package:crypto_address_checker/services/ocr_service.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'package:path_provider/path_provider.dart';

import '../state management/controller.dart';
import 'screenshot_input.dart';

class CropPage extends StatefulWidget {
  const CropPage({Key? key}) : super(key: key);

  @override
  _CropPageState createState() => _CropPageState();
}

class _CropPageState extends State<CropPage> {
  late Controller controller;
  late CropController _cropController;

  @override
  void initState() {
    // TODO: implement initState
    controller = Get.find();
    _cropController = CropController();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft
    ]);

    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        color: Colors.indigo,
        child: Container(
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                child: Icon(
                  Icons.crop_free,
                  color: Colors.white,
                  size: 30,
                ),
                onPressed: () {
                  // c.resetController();
                  // c.delete_image_cach();

                  _cropController.crop();
                },
              ),
            ],
          ),
        ),
      ),
      body: Container(
        child: Crop(
            initialArea: Rect.fromLTWH(140, 500, 800, 600),
            image: File(controller.picPathToCrop!).readAsBytesSync(),
            controller: _cropController,
            onCropped: (imageData) async {
              // do something with image data
              /*      String newFilePath =
=======
            onCropped: (image) async {
              // do something with image data
              String newFilePath =
>>>>>>> 95614ce37cdc776737c86b92ebbf97c6ab13b32d
                  '${controller.tempDirectory.value}/${DateTime.now().millisecondsSinceEpoch}.jpg';
              await EasyLoading.showToast("loading".tr, dismissOnTap: false);

              final file = await File(newFilePath).create();
              await file.writeAsBytes(image);
<<<<<<< HEAD
              String temp_path = (await getTemporaryDirectory()).path;

              String? result =
                  await OCR_process(await file.readAsBytes(), temp_path);

              print("ocr result " + result!);*/

              /*  switch (controller.platform_OS) {
                case 'macos':
                  controller.crop_page_Data.value =
                      "data:image/jpg;base64," + base64Encode(image);
                  break;

                case "android":
                  // do something with image data
                  String newFilePath =
                      '${controller.tempDirectory.value}/${DateTime.now().millisecondsSinceEpoch}.jpg';
                  await EasyLoading.showToast("loading".tr,
                      dismissOnTap: false);

                  final file = await File(newFilePath).create();
                  await file.writeAsBytes(image);
                  controller.crop_page_Data.value = file.path;*/

              switch (controller.picPathToCrop_mode) {
                case 'exchange':
                  // controller.exchangeAddressRecognitionResult.value =result;
                  controller.exchangeCroppedPicData.value = imageData;
                  break;

                case "wallet":
                  //     controller.walletAddressRecognitionResult.value = result;
                  controller.walletCroppedPicData.value = imageData;
                  break;
              }

              Get.to(() => ScreenShotPage());
            }

            //await controller.textDetectorController?.close();

            //EasyLoading.dismiss();

            ),
      ),
    );
  }
}
