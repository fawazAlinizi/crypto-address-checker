import 'dart:io';

import 'package:animate_do/animate_do.dart';

import 'package:file_picker/file_picker.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../state management/controller.dart';
import 'about_page.dart';
import 'adaPage.dart';
import 'address_input.dart';
import 'crop_page.dart';
import 'donation.dart';
import 'picture_view.dart';

class ScreenShotPage extends StatefulWidget {
  //Text_editor_controller?.text = controller.textRecognitionResult.value;

  @override
  _ScreenShotPageState createState() => _ScreenShotPageState();
}

class _ScreenShotPageState extends State<ScreenShotPage> {
  Controller controller = Get.find();
  AnimationController? pulseAnimateController;
  AnimationController? flashAnimateController;
  @override
  void initState() {
    // TODO: implement initState

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    controller = Get.find();
    super.initState();
  }

  @override
  void dispose() {
    pulseAnimateController?.dispose();
    flashAnimateController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          centerTitle: true,
          title: Text('Visual Checker'),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: TextButton(
                child: Icon(
                  Icons.text_fields_rounded,
                  size: 40,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.to(() => AddressPage());
                },
              ),
            )
          ],
        ),
        backgroundColor: Colors.indigo,
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Text(
                  "Info",
                  style: TextStyle(fontSize: 50, color: Colors.white),
                ),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                leading: Icon(
                  Icons.info,
                  color: Colors.black,
                  size: 50,
                ),
                title: Text(
                  "about",
                  style: TextStyle(fontSize: 30),
                ),
                onTap: () {
                  Get.back();

                  Get.to(() => aboutPage());
                },
              ),
              ListTile(
                title: Container(
                    child: Image.asset('assets/BuyMeACoffee_blue@2x.png')),
                onTap: () {
                  Get.back();
                  Get.to(() => DonationPage());
                },
              ),
              ListTile(
                title: Container(
                  width: 300,
                  height: 55,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Text(
                    "tip me some ADA",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
                onTap: () {
                  Get.back();
                  Get.to(() => AdaPage());
                },
              )
            ],
          ),
          // Populate the Drawer in the next step.
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        TextButton(
                          child: Icon(
                            Icons.crop,
                            color: Colors.white,
                            size: 40,
                          ),
                          onPressed: () {
                            controller.picPathToCropWithPlatform(
                                controller.exchange_address_pic.value,
                                'exchange');
                            Get.to(() => CropPage());
                          },
                        ),
                        Text(
                          'Exchange Address',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                        TextButton(
                          child: Icon(
                            Icons.image_outlined,
                            color: Colors.white,
                            size: 40,
                          ),
                          onPressed: () async {
                            if (Platform.isMacOS) {
                              FilePickerResult? result =
                                  await FilePicker.platform.pickFiles();

                              if (result != null) {
                                controller.exchange_address_pic.value =
                                    result.files.single.path!;

                                controller.exchangeCroppedPicData.value =
                                    await File(result.files.single.path!)
                                        .readAsBytes();
                              } else {
                                // User canceled the picker
                              }
                            } else {
                              controller.getImageFromGallery('exchange');
                            }
                          },
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 7),
                      width: screenWidth,
                      height: 80,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.black, width: 1),

                        // button text
                      ),
                      child: GetX<Controller>(builder: (con) {
                        return FutureBuilder<String>(
                          future: controller
                              .exchange_pic_recogntion_result_test, // async work
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                                return Center(
                                    child: CircularProgressIndicator());
                              default:
                                if (snapshot.hasError)
                                  return Text('Error: ${snapshot.error}');
                                else
                                  return SelectableText(
                                    snapshot.data.toString(),
                                    textAlign: TextAlign.left,
                                    style: TextStyle(),
                                  );
                            }
                          },
                        );
                      }),
                    ),
                    Container(
                      color: Colors.white,
                      child: TextButton(
                          child: Obx(
                            () => Image.file(
                                File(controller.exchange_address_pic.value),
                                fit: BoxFit.fitWidth,
                                width: screenWidth,
                                height: 250),
                          ),
                          onPressed: () {
                            if (Platform.isMacOS) {
                            } else {
                              controller.takePicForPlatform.value = 'exchange';
                              Get.to(() => TakePicturePage());
                            }
                          }),
                    ),
                  ],
                ),
                SizedBox(
                  height: 1,
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        TextButton(
                          child: Icon(
                            Icons.crop,
                            color: Colors.white,
                            size: 40,
                          ),
                          onPressed: () {
                            controller.picPathToCropWithPlatform(
                                controller.wallet_address_pic.value, 'wallet');

                            Get.to(() => CropPage());
                          },
                        ),
                        Text(
                          'Wallet Address',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                        TextButton(
                          child: Icon(
                            Icons.image_outlined,
                            color: Colors.white,
                            size: 40,
                          ),
                          onPressed: () async {
                            if (Platform.isMacOS) {
                              FilePickerResult? result =
                                  await FilePicker.platform.pickFiles();

                              if (result != null) {
                                controller.wallet_address_pic.value =
                                    result.files.single.path!;

                                controller.walletCroppedPicData.value =
                                    await File(result.files.single.path!)
                                        .readAsBytes();
                              } else {
                                // User canceled the picker
                              }
                            } else {
                              controller.getImageFromGallery('wallet');
                            }
                          },
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 7),
                      width: screenWidth,
                      height: 80,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.black, width: 1),

                        // button text
                      ),
                      child: GetX<Controller>(builder: (con) {
                        return FutureBuilder<String>(
                          future: controller
                              .wallet_pic_recogntion_result_test, // async work
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                                return Center(
                                    child: CircularProgressIndicator());
                              default:
                                if (snapshot.hasError)
                                  return Text('Error: ${snapshot.error}');
                                else
                                  return SelectableText(
                                    snapshot.data.toString(),
                                    textAlign: TextAlign.left,
                                    style: TextStyle(),
                                  );
                            }
                          },
                        );
                      }),
                    ),
                    Container(
                      color: Colors.white,
                      child: TextButton(
                          child: Obx(
                            () => Image.file(
                              File(controller.wallet_address_pic.value),
                              fit: BoxFit.cover,
                              width: screenWidth,
                              height: 250,
                            ),
                          ),
                          onPressed: () {
                            if (Platform.isMacOS) {
                            } else {
                              controller.takePicForPlatform.value = 'wallet';

                              Get.to(() => TakePicturePage());
                            }
                          }),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text(
                    'Do They Match? ',
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                  SizedBox(
                    width: 13,
                  ),
                  Pulse(
                    infinite: true,
                    controller: (controller) =>
                        pulseAnimateController = controller,
                    child: Flash(
                      infinite: true,
                      controller: (controller) =>
                          flashAnimateController = controller,
                      child: GetX<Controller>(builder: (controller) {
                        return Text(
                          '${controller.screenshotPageMatchingResult.value.toString()}',
                          style: TextStyle(
                              color:
                                  controller.screenshotPageMatchingResult.value
                                      ? Colors.green
                                      : Colors.red,
                              fontSize: 25),
                        );
                      }),
                    ),
                  )
                ]),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
