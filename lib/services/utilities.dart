import 'dart:io';

import 'package:flutter/services.dart';

Future<String> getImageFileFromAssets(String path, String tempDirectory) async {
  final byteData = await rootBundle.load('assets/$path');

  final file = File('${tempDirectory}/$path}');
  await file.writeAsBytes(byteData.buffer
      .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  return file.path;
}
