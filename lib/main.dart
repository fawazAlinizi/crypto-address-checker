import 'package:crypto_address_checker/pages/screenshot_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'package:responsive_framework/responsive_framework.dart';

import 'pages/about_page.dart';
import 'pages/adaPage.dart';
import 'pages/address_input.dart';
import 'pages/crop_page.dart';
import 'pages/donation.dart';
import 'pages/picture_view.dart';
import 'state management/controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

//initiate portrait mode
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    Get.put(Controller());

    runApp(MyApp());
  });
}

//initialized in the main

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var introduction;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        builder: EasyLoading.init(
            builder: (context, widget) => ResponsiveWrapper.builder(
                  BouncingScrollWrapper.builder(context, widget!),
                  maxWidth: MediaQuery.of(context).size.width *
                      MediaQuery.of(context).devicePixelRatio,
                  minWidth: 500,
                  defaultScale: true,
                  breakpoints: [
                    ResponsiveBreakpoint.autoScale(500, name: DESKTOP),
                    ResponsiveBreakpoint.autoScale(800, name: TABLET),
                    //ResponsiveBreakpoint.autoScale(150, name: DESKTOP),
                  ],
                )),
        debugShowCheckedModeBanner: false,
        getPages: [
          GetPage(name: '/address_input_page', page: () => AddressPage()),
          GetPage(name: '/screenshot_input_page', page: () => ScreenShotPage()),
          GetPage(name: '/crop_page', page: () => CropPage()),
          GetPage(name: '/camera_page', page: () => TakePicturePage()),
          GetPage(name: '/aboutPage_page', page: () => aboutPage()),
          GetPage(name: '/AdaPage_page', page: () => AdaPage()),
          GetPage(name: '/DonationPage_page', page: () => DonationPage()),
        ],
        home: AddressPage());
  }
}
