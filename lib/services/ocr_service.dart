import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:http/http.dart' as http;
/*
Future<File> create_file_from_bytes(bytes_data, String newFile_path) async {
  final file =
      await File('$newFile_path/${DateTime.now().millisecondsSinceEpoch}.png')
          .create();
  await file.writeAsBytes(bytes_data);
  return file;
}

Future<String?> OCR_process(
    Uint8List CroppedImagePath_data, String tempPath) async {
  final file = await create_file_from_bytes(CroppedImagePath_data, tempPath);

  final inputImage = InputImage.fromFile(file);

  TextDetector _textDetector = GoogleMlKit.vision.textDetector();
  //textDetectorController = _textDetector;

  final visionText = await _textDetector.processImage(inputImage);
  print('ok this the text recognition result...' + visionText.text);
  _textDetector.close();
  file.deleteSync();
  return visionText.text;
}*/

/*Future<String> OCR_process(String image_base64_string) async {
  Uri url = Uri.parse('https://api.ocr.space/parse/image');
  var payload = {"base64Image": "$image_base64_string"};
  var header = {"apikey": "6aaa1c7d8d88957"};
  var post = await http.post(url = url, body: payload, headers: header);
  var result = jsonDecode(post.body);
  print(result['ParsedResults'][0]['ParsedText']);
  return result['ParsedResults'][0]['ParsedText'];
}*/

//this ocr used with google ml
/*
Future<String> OCR_process(String picPath) async {
  final inputImage = InputImage.fromFile(File(picPath));
  print("step 1");
  TextDetector _textDetector = GoogleMlKit.vision.textDetector();
  // textDetectorController = _textDetector;
  print("step 2");

  final visionText = await _textDetector.processImage(inputImage);
  print("step 3");

  print('ok this the text recognition result...' + visionText.text);
  String text = '';
  for (TextBlock block in visionText.blocks) {
    for (TextLine line in block.lines) {
      // Same getters as TextBlock
      for (TextElement element in line.elements) {
        text += element.text;
      }
    }
  }
  print('ok this the text recognition result...' + text);

  _textDetector.close();
  return text;
}*/

Future<String> OCR_process(var data, String tempDirectory) async {
  if (Platform.isMacOS) {
    try {
      String base65Data = "data:image/jpg;base64," + base64Encode(data);
      Uri url = Uri.parse('https://api.ocr.space/parse/image');
      var payload = {"base64Image": "$base65Data"};
      var header = {"apikey": "6aaa1c7d8d88957"};

      var post = await http.post(url = url, body: payload, headers: header);
      print("its ...." + post.statusCode.toString());
      if (post.statusCode == 200) {
        print("its 200");
        var result = jsonDecode(post.body);

        //  print("request code is" + post.statusCode.toString());
        print(result['ParsedResults'][0]['ParsedText']);
        return result['ParsedResults'][0]['ParsedText'];
      } else {
        throw "Status Code ${post.statusCode.toString()}";
      }
    } catch (e) {
      throw "Request error: " + e.toString();
    }
  } else {
    String newFilePath =
        '${tempDirectory}/${DateTime.now().millisecondsSinceEpoch}.png';

    final file = await File(newFilePath).create();
    await file.writeAsBytes(data);
    // controller.crop_page_Data.value = file.path;

    final inputImage = InputImage.fromFile(File(file.path));
    print("step 1");
    TextDetector _textDetector = GoogleMlKit.vision.textDetector();
    // textDetectorController = _textDetector;
    print("step 2");

    final visionText = await _textDetector.processImage(inputImage);
    print("step 3");

    print('ok this the text recognition result...' + visionText.text);
    String text = '';
    for (TextBlock block in visionText.blocks) {
      for (TextLine line in block.lines) {
        // Same getters as TextBlock
        for (TextElement element in line.elements) {
          text += element.text;
        }
      }
    }
    print('ok this the text recognition result...' + text);

    _textDetector.close();
    return text;
  }
}
